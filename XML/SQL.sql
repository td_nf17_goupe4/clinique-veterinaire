CREATE TABLE Classes(
nom VARCHAR(50),
PRIMARY KEY (nom)
);


CREATE TABLE Especes(
nom VARCHAR(50),
classe VARCHAR(50) NOT NULL,
PRIMARY KEY (nom),
FOREIGN KEY (classe) REFERENCES Classes(nom)
);



CREATE TABLE Clients(
id INTEGER,
telephone INTEGER NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999)
);


CREATE TABLE Animaux(
id INTEGER,
nom VARCHAR(30) NOT NULL,
dernierPoids INTEGER  NOT NULL,
derniereTaille INTEGER  NOT NULL,
date_naissance INTEGER  NOT NULL,
espece VARCHAR(50) NOT NULL,
traitement XMLType,
PRIMARY KEY (id),
FOREIGN KEY (espece) REFERENCES Especes(nom),
CHECK (dernierPoids > 0),
CHECK (derniereTaille > 0)
);


CREATE TABLE Proprietaires(
client INTEGER,
animal INTEGER,
PRIMARY KEY (client, animal),
FOREIGN KEY (client) REFERENCES Clients(id),
FOREIGN KEY (animal) REFERENCES Animaux(id)
);


