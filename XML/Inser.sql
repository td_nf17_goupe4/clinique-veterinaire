INSERT INTO Classes VALUES('Mammiferes');
INSERT INTO Classes VALUES('Poissons');
INSERT INTO Classes VALUES('Oiseaux');
INSERT INTO Classes VALUES('Reptiles');

INSERT INTO Especes VALUES ('Chien', 'Mammiferes');
INSERT INTO Especes VALUES ('Anaconda', 'Reptiles');
INSERT INTO Especes VALUES ('Moineau', 'Oiseaux');



INSERT INTO Clients VALUES(1, '0610203050', 'Dupond', 'Anne', TO_DATE('1995-07-14', 'YYYY-MM-DD'), '50 rue de Nantes 75015 Paris');
INSERT INTO Clients VALUES(2, '0610203050', 'Dupond', 'Anne', TO_DATE('1995-07-14', 'YYYY-MM-DD'), '50 rue de Nantes 75015 Paris');
INSERT INTO Clients VALUES(3, '0614814050', 'Smith', 'Louis', TO_DATE('1980-10-28', 'YYYY-MM-DD'), '25 rue Saint Anne 75009 Paris');


INSERT INTO Animaux VALUES (1, 'Dylan', 50, 70, 2007, 'Chien', XMLType('
<traitements>
    <traitement>
        <nom>Kiki</nom>
        <debut>2019-12-05</debut>
        <duree>2</duree>
            <prescription>
                <quantite_jour>5</quantite_jour>
                <medicament>
                    <nom_molecule>BHRG</nom_molecule>
                    <effets>anti poil</effets>
                </medicament>
            </prescription>
        <veterinaire>
            <nom>Jacky</nom>
            <prenom>Elsa</prenom>
            <telephone>07895623</telephone>
            <date_naissance>1986-12-03</date_naissance>
            <adresse>30 rue des pommiers Paris</adresse>
        </veterinaire>
    </traitement>
    <traitement>
        <nom>Kiki</nom>
        <debut>2019-06-05</debut>
        <duree>1</duree>
            <prescription>
                <quantite_jour>1</quantite_jour>
                <medicament>
                    <nom_molecule>hggh</nom_molecule>
                    <effets>no effect</effets>
                </medicament>
            </prescription>
        <veterinaire>
            <nom>Jacky</nom>
            <prenom>Elsa</prenom>
            <telephone>07895623</telephone>
            <date_naissance>1986-12-03</date_naissance>
            <adresse>30 rue des pommiers Paris</adresse>
        </veterinaire>
    </traitement>
</traitements>
'));

INSERT INTO Animaux VALUES (2, 'Hector', 70, 15, 2015, 'Moineau', XMLType('
<traitements>
    <traitement>
        <nom>Salem</nom>
        <debut>2019-12-05</debut>
        <duree>2</duree>
        <veterinaire>
            <nom>Michel</nom>
            <prenom>Ramy</prenom>
            <telephone>07895897</telephone>
            <date_naissance>1958-09-12</date_naissance>
            <adresse>12 rue des pommiers Paris</adresse>
        </veterinaire>
    </traitement>
</traitements>
'));

INSERT INTO Animaux VALUES (3, 'James', 20, 350, 2008, 'Anaconda', XMLType('
<traitements>
    <traitement>
        <nom>Kiki</nom>
        <debut>2019-12-05</debut>
        <duree>2</duree>
            <prescription>
                <quantite_jour>2</quantite_jour>
                <medicament>
                    <nom_molecule>TQKJ</nom_molecule>
                    <effets>anti puce</effets>
                </medicament>
            </prescription>
            <prescription>
                <quantite_jour>3</quantite_jour>
                <medicament>
                    <nom_molecule>KHT6F</nom_molecule>
                    <effets>detartrant</effets>
                </medicament>
            </prescription>
        <veterinaire>
            <nom>Jacky</nom>
            <prenom>Elsa</prenom>
            <telephone>07895623</telephone>
            <date_naissance>1986-12-03</date_naissance>
            <adresse>30 rue des pommiers Paris</adresse>
        </veterinaire>
    </traitement>
</traitements>
'));


INSERT INTO Proprietaires VALUES (2, 2);
INSERT INTO Proprietaires VALUES (1, 3);
INSERT INTO Proprietaires VALUES (2, 1);

