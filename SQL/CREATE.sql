CREATE TABLE Classes(
nom VARCHAR(50),
PRIMARY KEY (nom)
);


CREATE TABLE Especes(
nom VARCHAR(50),
classe VARCHAR(50) NOT NULL,
PRIMARY KEY (nom),
FOREIGN KEY (classe) REFERENCES Classes(nom)
);


CREATE TABLE Clients(
id INTEGER,
telephone INTEGER NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999),
CHECK (date_naissance BETWEEN DATE('1900-01-01') AND CURRENT_DATE)
);

CREATE TABLE Veterinaires(
id INTEGER,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
specialite VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (specialite) REFERENCES Classes(nom),
CHECK (telephone > 100000000 AND telephone < 999999999),
CHECK (date_naissance BETWEEN DATE('1900-01-01') AND CURRENT_DATE)
);

CREATE TABLE Assistants(
id INTEGER,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
specialite VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (specialite) REFERENCES Classes(nom),
CHECK (telephone > 100000000 AND telephone < 999999999),
CHECK (date_naissance BETWEEN DATE('1900-01-01') AND CURRENT_DATE)
);




CREATE TABLE Animaux(
id INTEGER,
nom VARCHAR(30) NOT NULL,
dernierPoids INTEGER  NOT NULL,
derniereTaille INTEGER  NOT NULL,
date_naissance INTEGER,
espece VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (espece) REFERENCES Especes(nom),
CHECK (dernierPoids > 0),
CHECK (derniereTaille > 0),
CHECK (date_naissance > 1900)
--CHECK(date_naissance < extract(YEAR FROM CURRENT_DATE))
);


CREATE TABLE Proprietaires(
client INTEGER,
animal INTEGER,
PRIMARY KEY (client, animal),
FOREIGN KEY (client) REFERENCES Clients(id),
FOREIGN KEY (animal) REFERENCES Animaux(id)
);


CREATE TABLE Medicaments(
nomMolecule VARCHAR(50),
effets VARCHAR(255) NOT NULL,
PRIMARY KEY (nomMolecule)
);


CREATE TABLE Autorise(
medicament VARCHAR(50),
espece VARCHAR(50),
PRIMARY KEY (medicament, espece),
FOREIGN KEY (medicament) REFERENCES Medicaments(nomMolecule),
FOREIGN KEY (espece) REFERENCES Especes(nom)
);

CREATE TABLE Traitements(
id INTEGER,
nom VARCHAR(100), 
debut DATE NOT NULL,
duree INTEGER NOT NULL,
veterinaire INTEGER NOT NULL,
animal INTEGER NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (veterinaire) REFERENCES Veterinaires(id),
FOREIGN KEY (animal) REFERENCES Animaux(id),
--CHECK (debut >= CURRENT_DATE),
CHECK (duree > 0)
);


CREATE TABLE Prescriptions(
traitement INTEGER,
medicament VARCHAR(50),
quantite INTEGER  NOT NULL,
PRIMARY KEY (traitement, medicament),
FOREIGN KEY (medicament) REFERENCES Medicaments(nomMolecule),
FOREIGN KEY (traitement) REFERENCES Traitements(id),
CHECK (quantite > 0)
);

CREATE VIEW vClients AS
SELECT telephone, nom, prenom, date_naissance, adresse
FROM Clients
;

CREATE VIEW vPersonnel AS
SELECT telephone, nom, prenom, date_naissance, adresse, specialite
FROM Assistants 
UNION  
SELECT telephone, nom, prenom, date_naissance, adresse, specialite
FROM Veterinaires
;


CREATE VIEW vPersonne AS
SELECT telephone, nom, prenom, date_naissance, adresse
FROM vClients
UNION
SELECT telephone, nom, prenom, date_naissance, adresse
FROM vPersonnel
;

-- CONTRAINTES


CREATE VIEW Check_Especes_Classes AS
SELECT C.nom
FROM Classes C LEFT JOIN Especes E 
ON C.nom=E.classe
WHERE E.classe IS NULL;

CREATE VIEW Check_Animal_Proprietaires AS
SELECT A.id
FROM Animaux A LEFT JOIN Proprietaires P
ON A.id=P.animal
WHERE P.animal IS NULL;

CREATE VIEW Check_Client_Proprietaires AS
SELECT C.id
FROM Clients C LEFT JOIN Proprietaires P
ON C.id=P.client
WHERE P.client IS NULL;

CREATE VIEW Check_Personnel_Specialite AS
SELECT C.nom
FROM Classes C LEFT JOIN (SELECT specialite FROM Veterinaires UNION SELECT specialite FROM Assistants) AS P
ON C.nom=P.specialite
WHERE P.specialite IS NULL;

CREATE VIEW Check_Prescriptions_Traitements AS
SELECT P.traitement
FROM Prescriptions P LEFT JOIN Traitements T
ON P.traitement=T.id
WHERE T.id IS NULL;


CREATE VIEW Check_Medicaments_Autorise AS
SELECT M.nomMolecule
FROM Medicaments M LEFT JOIN Autorise A
ON M.nomMolecule=A.medicament
WHERE A.medicament IS NULL;
