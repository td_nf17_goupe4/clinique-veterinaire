INSERT INTO Classes(nom)
VALUES
('Mammiferes'),
('Poissons'),
('Oiseaux'),
('Reptiles');

INSERT INTO Especes(nom, classe)
VALUES
('Chat', 'Mammiferes'),
('Chien', 'Mammiferes'),
('Anaconda', 'Reptiles'),
('Moineau', 'Oiseaux');





INSERT INTO Clients (id, telephone, nom, prenom, date_naissance, adresse)
VALUES
(1, '0610203050', 'Dupond', 'Anne', '1995-07-14', '50 rue de Nantes 75015 Paris'),
(2, '0610203050', 'Dupond', 'Anne', '1995-07-14', '50 rue de Nantes 75015 Paris'),
(3, '0614814050', 'Smith', 'Louis', '1980-10-28', '25 rue Saint Anne 75009 Paris');

INSERT INTO Veterinaires (id, telephone, nom, prenom, date_naissance, adresse, specialite)
VALUES
(1, '0616814835', 'Martin', 'John', '1970-01-20', '10 rue Saint Anne 75009 Paris', 'Oiseaux'),
(2, '0616484255', 'Martin', 'Alex', '1987-05-21', '15 faubourg Saint Martin 60200 Compiegne', 'Mammiferes'),
(3, '0770028656', 'Bruley', 'Philippe', '1973-05-31', '20 rue de la gare 60200 Compiegne', 'Reptiles');



INSERT INTO Assistants (id, telephone, nom, prenom, date_naissance, adresse, specialite)
VALUES
(3, '0616445424', 'Dubois', 'Thomas', '1980-08-10', '15 faubourg Saint Martin 75010 Paris', 'Oiseaux'),
(4, '0854345689', 'Picard', 'Jacques', '1920-12-12', '19 rue Saint Gervais 60700 Pontpoint', 'Mammiferes'),
(5, '0536608783', 'Bouvier', 'Mathieu', '2000-01-10', '2 rue du Dahomey 60200 Compiegne', 'Poissons');

INSERT INTO Animaux(id, nom, dernierPoids, derniereTaille, date_naissance, espece)
VALUES
(1, 'Toutou', 50, 70, 2007, 'Chien'),
(2, 'Hector', 70, 15, 2015, 'Moineau'),
(3, 'James', 20, 350, 2008, 'Anaconda'),
(4, 'Luna', 15, 95, 2003, 'Chien'),
(5, 'Cacao', 10, 60, 2016, 'Chat'),
(6, 'Noisettes', 10, 60, 2013, 'Chat');


INSERT INTO Proprietaires(client, animal)
VALUES
(2, 2),
(1, 3),
(2, 1),
(2, 5),
(3, 4),
(3, 6);

INSERT INTO Medicaments(nomMolecule, effets)
VALUES
('CH4O3', 'Créé des démangeaisons dans le cou'),
('AgC3', 'Facilite le transit intestinal'),
('Au4Co2H2O', 'Tue les poux'),
('Au', 'Rend riche'),
('Co2', 'Pollue');


INSERT INTO Autorise(medicament, espece)
VALUES
('CH4O3', 'Chien'),
('CH4O3', 'Anaconda'),
('CH4O3', 'Moineau'),
('CH4O3', 'Chat'),
('AgC3', 'Chien'),
('AgC3', 'Anaconda'),
('AgC3', 'Moineau'),
('AgC3', 'Chat'),
('Au4Co2H2O', 'Chien'),
('Au4Co2H2O', 'Anaconda'),
('Au4Co2H2O', 'Moineau'),
('Au4Co2H2O', 'Chat'),
('Au', 'Chien'),
('Au', 'Anaconda'),
('Au', 'Moineau'),
('Au', 'Chat'),
('Co2', 'Chien'),
('Co2', 'Anaconda'),
('Co2', 'Moineau'),
('Co2', 'Chat');


INSERT INTO Traitements(id, nom, debut, duree, veterinaire, animal)
VALUES
(1,'Traitement contre les poux', '2019-05-30', 30, 1, 1),
(2,'Traitement pour la diarrhée', '2019-03-29', 10, 1, 2),
(3,'Mort aux rats', '2019-12-12', 115, 3, 1),
(13,'Antiseptique', '2019-03-29', 11, 1, 2),
(4,'Antistaminique', '2019-03-29', 121, 2, 2),
(5,'Contre le poil à gratter', '2019-03-29', 3, 1, 2),
(6,'Contre la fièvre', '2019-03-29', 5, 2, 6),
(7,'Contre la fièvre', '2019-03-29', 5, 2, 6),
(8,'Contre les chutes de poils', '2019-03-29', 55, 1, 2),
(9,'Contre les chutes de poils', '2019-03-29', 55, 1, 2),
(10,'Contre les chutes de poils', '2019-03-29', 55, 1, 2),
(11,'Contre les chutes de poils', '2019-03-29', 55, 1, 6),
(12,'Pour renforcer la libido', '2019-03-29', 22, 2, 2);



INSERT INTO Prescriptions(traitement, medicament, quantite)
VALUES
(1, 'CH4O3', 5),
(2, 'Au4Co2H2O', 2),
(1, 'AgC3', 8),
(3, 'Au', 2),
(3, 'Co2', 2),
(4, 'Au4Co2H2O', 1),
(5, 'Au', 6),
(8, 'Au4Co2H2O', 2),
(7, 'Co2', 3),
(2, 'Au', 5),
(12, 'Au', 4),
(12, 'Co2', 10),
(11, 'Co2', 9),
(10, 'Au4Co2H2O', 1),
(9, 'Au4Co2H2O', 1);
