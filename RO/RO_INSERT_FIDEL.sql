INSERT INTO Classe
VALUES ('Mammifere');
INSERT INTO Classe
VALUES ('Oiseau');

INSERT INTO Espece
VALUES ('Chien', 'Mammifere');
INSERT INTO Espece
VALUES ('Baleine', 'Mammifere');
INSERT INTO Espece
VALUES ('Moineau', 'Oiseau');


INSERT INTO Animaux
VALUES (1, 'Jean michel', 10, 50, 'Chien', TO_DATE('2018-02-12', 'YYYY-MM-DD'), 
LTraitement(
    TyTraitement('vaccin anti facebook',TO_DATE('2018-02-12', 'YYYY-MM-DD'), 12, 'vincent', 'PHR-15K', 1),
    TyTraitement('vaccin anti twitter',TO_DATE('2018-02-24', 'YYYY-MM-DD'), 12, 'vincent', 'PHK-9969', 2)
));

INSERT INTO Animaux
VALUES (2, 'kevin', 10, 50, 'Baleine', TO_DATE('2017-03-12', 'YYYY-MM-DD'), 
LTraitement(
    TyTraitement('anti-poil',TO_DATE('2019-02-12', 'YYYY-MM-DD'), 12, 'AnetA8', 'OUIL', 1),
    TyTraitement('anti-patte-tic',TO_DATE('2019-02-24', 'YYYY-MM-DD'), 12, 'AnetA8', 'OUIL', 2)
));


INSERT INTO Clients
VALUES (1, 'Bond', 'James', TO_DATE('12-04-1987', 'DD-MM-YYYY'), 0685862154, '50 rue de Champs Elysées - 75000'); 

INSERT INTO Clients
VALUES (2, 'Dupont', 'Anne', TO_DATE('14-07-1995', 'DD-MM-YYYY'), 0610203050, '50 rue de Nantes - 75015');

INSERT INTO Clients
VALUES (3, 'Smith', 'Louis', TO_DATE('28-10-1980', 'DD-MM-YYYY'), 0614814050, '25 rue Saint Anne - 75009');


INSERT INTO Proprietaires VALUES (1,1);
INSERT INTO Proprietaires VALUES (2,2);
INSERT INTO Proprietaires VALUES (3,2);

