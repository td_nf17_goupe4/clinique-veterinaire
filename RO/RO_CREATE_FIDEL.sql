-- Si on choisi de garder une grande fidilité par rapport au sujet du projet, et cela permet de préserver au maximum les contraintes. 
-- Nous avions fait le choix qu'un animal puisse appartenir à 2 personnes, donc nous devons garder Animal en tant que TABLE 
-- Plusiuers Animaux peuvent avoir le même proprietaire(client) donc il est également préferable de garder une TABLE pour Client
-- Nous avons également décidé de garder Classe et Espèce sous forme de table pour le referencement dans la table des Animaux
-- Nous supposons que les traitements n'ont pas besoin d'être référencés donc nous en faisons un type 


CREATE TABLE Classe(
nom varchar(50) PRIMARY KEY);
/

CREATE TABLE Espece(
nom varchar(50) PRIMARY KEY,
classe varchar(50) NOT NULL,
FOREIGN KEY (classe) REFERENCES Classe(nom));
/


CREATE OR REPLACE TYPE TyTraitement AS OBJECT(
nom varchar2(255),
debut date,
duree integer,
veterinaire varchar2(50),
medicament varchar2(50),
quantiteJour integer);
/

CREATE OR REPLACE TYPE LTraitement AS TABLE OF TyTraitement;
/


CREATE TABLE Animaux(
id integer PRIMARY KEY,
nom varchar2(50),
dernierPoids integer NOT NULL,
derniereTaille integer NOT NULL,
espece varchar2(50) NOT NULL,
dateNaissance date,
traitement LTraitement,
FOREIGN KEY (espece) REFERENCES Espece(nom),
CHECK (dernierPoids > 0),
CHECK (derniereTaille > 0))
NESTED TABLE traitement STORE AS ntTraitement;
/

CREATE TABLE Clients(
id integer PRIMARY KEY,
nom varchar2(50),
prenom varchar2(50),
dateNaissance date,
telephone integer NOT NULL,
adresse varchar2(255) NOT NULL,
CHECK (telephone > 100000000 AND telephone < 999999999));
/


CREATE TABLE Proprietaires(
id_client integer,
id_animal integer,
PRIMARY KEY (id_client, id_animal),
FOREIGN KEY (id_client) REFERENCES Clients(id),
FOREIGN KEY (id_animal) REFERENCES Animaux(id));
/
