-- Ce model utilise beaucoup de types object mais c'est au sacrifice de nombreuse contraintes. Nous avons donc décidé de ne pas faire d'inserstion pour ce model
--Ici on fait l'hypothèse que que nous n'avons pas besoin de besoin de réferencer les traitements et les animaux dans d'autres tables, 
--Nous avons également fait le choix de dire qu'un animal n'appartient qu'à un seul client
--En créant des types on ajoute de la redondance et cela enleve des contraintes qu'il faudra assurer au niveau de la couche applicative :
--TyTraitement : tous les attributs NOT NULL et debut,duree > 0
--TyAnimal : tous les attributs NOT NULL et derniereTaille,dernierpoids > 0

CREATE OR REPLACE TYPE TyTraitement AS OBJECT(
nom varchar2(255),
debut date,
duree integer,
veterinaire varchar2(50),
medicament varchar2(50),
quantiteJour integer);
/

CREATE OR REPLACE TYPE LTraitement AS TABLE OF TyTraitement;
/


CREATE OR REPLACE TYPE TyClasse AS OBJECT(
nom varchar2(255));
/

CREATE OR REPLACE TYPE TyEspece AS OBJECT(
nom varchar2(255),
classe TyClasse);
/


CREATE OR REPLACE TYPE TyAnimal AS OBJECT(
nom varchar2(50),
dernierPoids integer,
derniereTaille integer,
dateNaissance date,
traitement LTraitement,
espece TyEspece);
/

CREATE OR REPLACE TYPE LAnimaux AS TABLE OF TyAnimal;
/

CREATE TABLE Clients (
nom varchar2(50),
prenom varchar2(50),
dateNaissance date,
telephone integer NOT NULL,
adresse varchar2(255) NOT NULL,
animaux LAnimaux,
CHECK (telephone > 100000000 AND telephone < 999999999))
NESTED TABLE animaux STORE AS ntAnimaux (NESTED TABLE traitement STORE AS ntTraitement);
/

