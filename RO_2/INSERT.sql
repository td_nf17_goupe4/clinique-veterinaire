DECLARE

RefClasse1 REF TClasse;

RefEspece1 REF TEspece;
RefEspece2 REF TEspece;

RefMedicament1 REF TMedicament;
RefMedicament2 REF TMedicament;

RefTraitement1 REF TTraitement;
RefTraitement2 REF TTraitement;
RefTraitement3 REF TTraitement;


BEGIN


INSERT INTO Classe VALUES ('mammifère');
INSERT INTO Classe VALUES ('reptile');

--init RefClasse1
SELECT REF(C) INTO RefClasse1 FROM Classe C WHERE nom='mammifère';

INSERT INTO Espece VALUES ('chien', RefClasse1);
INSERT INTO Espece VALUES ('chat', RefClasse1);

--init RefEspece1 & RefEspece2
SELECT REF(E) INTO RefEspece1 FROM Espece E WHERE nom='chien';
SELECT REF(E) INTO RefEspece2 FROM Espece E WHERE nom='chat';

INSERT INTO Medicament VALUES ('BHRG', 'anti puce', 
	ListRefEspece(
		TRefEspece(RefEspece1), 
		TRefEspece(RefEspece2)
	)
);

INSERT INTO Medicament VALUES ('KHFG', 'calmant', ListRefEspece(TRefEspece(RefEspece1)));

--init RefMedicament1 & RefMedicament2
SELECT REF(M) INTO RefMedicament1 FROM Medicament M WHERE nomMolecule='BHRG';
SELECT REF(M) INTO RefMedicament2 FROM Medicament M WHERE nomMolecule='KHFG';


INSERT INTO Traitement VALUES
(
  1, 'traitement total', TO_DATE('2019-06-10','YYYY-MM-DD'),5,
    ListPrescription(
        TPrescription(RefMedicament1, 2),
        TPrescription(RefMedicament2, 1)
    )
);

INSERT INTO Traitement VALUES
(
  2, 'traitement calmant', TO_DATE('2019-06-10','YYYY-MM-DD'),3,
    ListPrescription(TPrescription(RefMedicament2, 1 ))
);

INSERT INTO Traitement VALUES
(
  3, 'traitement anti puce', TO_DATE('2019-06-10','YYYY-MM-DD'),4,
    ListPrescription(TPrescription(RefMedicament1, 2 ))
);


--Init RefTraitement1 & RefTraitement2 & RefTraitement3
SELECT REF(T) INTO RefTraitement1 FROM Traitement T WHERE id=1;
SELECT REF(T) INTO RefTraitement2 FROM Traitement T WHERE id=2;
SELECT REF(T) INTO RefTraitement3 FROM Traitement T WHERE id=3;


INSERT INTO Clients VALUES
(
1, 0789653214, 'bond', 'james', TO_DATE('1985-01-12', 'YYYY-MM-DD'), '150 avenue de Paris',
  LAnimaux(TAnimal(
        1,'lechien', 15, 30,1999, RefEspece1,
        ListRefTraitement(TRefTraitement(RefTraitement1), TRefTraitement(RefTraitement2))
    ),
    TAnimal(
        2,'lechat', 12, 10,1999, RefEspece2,
        ListRefTraitement(TRefTraitement(RefTraitement3))
    ))
);


INSERT INTO Veterinaires VALUES
(1, '0616814835', 'Martin', 'John', TO_DATE('1970-01-20', 'YYYY-MM-DD'), '10 rue Saint Anne 75009 Paris', RefClasse1,
  ListRefTraitement(TRefTraitement(RefTraitement1), TRefTraitement(RefTraitement2))
);

INSERT INTO Veterinaires VALUES
(2, '0616814835', 'Anna', 'Alston',TO_DATE( '1980-01-20', 'YYYY-MM-DD'), '10 rue De gaule 75009 Paris', RefClasse1,
  ListRefTraitement(TRefTraitement(RefTraitement3))
);


INSERT INTO Assistants VALUES
(3, '0616445424', 'Dubois', 'Thomas',TO_DATE('1980-08-10', 'YYYY-MM-DD') , '15 faubourg Saint Martin 75010 Paris', RefClasse1);
INSERT INTO Assistants VALUES
(4, '0854345689', 'Picard', 'Jacques',TO_DATE('1920-12-12', 'YYYY-MM-DD') , '19 rue Saint Gervais 60700 Pontpoint', RefClasse1);


END;
/