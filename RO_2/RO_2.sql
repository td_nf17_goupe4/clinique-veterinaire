CREATE OR REPLACE TYPE TClasse AS OBJECT (
nom VARCHAR(50)
);
/
CREATE TABLE Classe OF Tclasse (
PRIMARY KEY (nom)
);
/

CREATE OR REPLACE TYPE TEspece AS OBJECT(
nom VARCHAR(50),
classe REF TClasse
);
/
CREATE TABLE Espece OF TEspece (
PRIMARY KEY (nom),
SCOPE FOR (classe) IS Classe
);
/
CREATE OR REPLACE TYPE TRefEspece AS OBJECT(espece REF TEspece);
/
CREATE TABLE RefEspece OF TRefEspece( SCOPE FOR (espece) IS Espece);
/
CREATE OR REPLACE TYPE ListRefEspece AS TABLE OF TRefEspece;
/

------------------------------------------------------



CREATE OR REPLACE TYPE TMedicament AS OBJECT(
nomMolecule VARCHAR(50),
effets VARCHAR(255),
especes ListRefEspece
);
/
CREATE TABLE Medicament OF TMedicament(
PRIMARY KEY (nomMolecule),
effets NOT NULL
) NESTED TABLE especes STORE AS NT_Medicament_Especes;
/
CREATE OR REPLACE TYPE TPrescription AS OBJECT(
  medicament REF TMedicament,
  quantiteJour integer
);
/
CREATE TABLE Prescription OF TPrescription(
quantiteJour NOT NULL,
CHECK(quantiteJour > 0),
SCOPE FOR (medicament) IS Medicament
);
/
CREATE OR REPLACE TYPE ListPrescription AS TABLE OF TPrescription;
/

------------------------------------------------------

CREATE OR REPLACE TYPE TTraitement AS OBJECT(
id integer,
nom varchar2(255),
debut date,
duree integer,
prescriptions ListPrescription
);
/
CREATE TABLE Traitement OF TTraitement(
  PRIMARY KEY (id),
  nom NOT NULL,
  debut NOT NULL,
  duree NOT NULL,
  CHECK (duree > 0)
) NESTED TABLE prescriptions STORE AS NT_Traitement_Prescriptions;

CREATE OR REPLACE TYPE TRefTraitement AS OBJECT(traitement REF TTraitement);
/
CREATE TABLE RefTraitement OF TRefTraitement( SCOPE FOR (traitement) IS Traitement);
/
CREATE OR REPLACE TYPE ListRefTraitement AS TABLE OF TRefTraitement;
/

 ----------------------------------------------------------------



 CREATE OR REPLACE TYPE TAnimal AS OBJECT (
 id INTEGER,
 nom VARCHAR(30),
 dernierPoids INTEGER,
 derniereTaille INTEGER,
 date_naissance INTEGER,
 espece REF TEspece,
 traitements ListRefTraitement
 );
/
CREATE TYPE LAnimaux AS TABLE OF TAnimal;
/


--CREATE TABLE Animal OF TAnimal(
--nom NOT NULL,
--dernierPoids NOT NULL,
--derniereTaille NOT NULL,
--date_naissance NOT NULL,
---PRIMARY KEY (id),
--FOREIGN KEY (espece) REFERENCES Especes(nom),
---CHECK (dernierPoids > 0),
---CHECK (derniereTaille > 0),
---CHECK (date_naissance > 1900),
---espece NOT NULL,
--SCOPE FOR espece IS Espece
--) NESTED TABLE traitements STORE AS NT_Animal_Traitements;
--/


CREATE TABLE Clients(
id INTEGER,
telephone INTEGER NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
animaux LAnimaux,
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999)
) NESTED TABLE animaux STORE AS NT_Client_Animaux (NESTED TABLE traitements STORE AS NT_Animal_Traitements);
/

CREATE TABLE Veterinaires(
id INTEGER,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
specialite REF TClasse NOT NULL,
traitements ListRefTraitement,
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999),
SCOPE FOR (specialite) IS Classe
) NESTED TABLE traitements STORE AS NT_Veterinaire_Traitements;
/

CREATE TABLE Assistants(
id INTEGER,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255) NOT NULL,
specialite REF TClasse NOT NULL,
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999),
SCOPE FOR (specialite) IS Classe
);
/
