INSERT INTO Classes VALUES('Mammiferes');
INSERT INTO Classes VALUES('Poissons');
INSERT INTO Classes VALUES('Oiseaux');
INSERT INTO Classes VALUES('Reptiles');

INSERT INTO Especes VALUES ('Chien', 'Mammiferes');
INSERT INTO Especes VALUES ('Baleine', 'Mammiferes');
INSERT INTO Especes VALUES ('Anaconda', 'Reptiles');
INSERT INTO Especes VALUES ('Moineau', 'Oiseaux');




INSERT INTO Animal values(
  1,
  'Volt',
  15,
  200,
  2008,
  'Chien',
  '[
      {"nom" : "Traitement contre les poux", "debut" : "20 janvier 2019", "duree" : 130,
          "prescription" : [
              {"quantité" : 15, "medicament" : {"nomMolecule" : "BHRG", "effet" : "tue les poux"}},
              {"quantité" : 2, "medicament" : {"nomMolecule" : "HO76", "effet" : "ne fait rien"}}
          ],
          "veterinaire" : {"nom" : "Richard", "prenom" : "Emmanuel"}
      },


      {"nom" : "Traitement pour le débarquement", "debut" : "6 juin 1944", "duree" : 67390, 
          "prescription" : [
              {"quantité" : 20, "medicament" : {"nomMolecule" : "JZYA", "effet" : "Remonte le moral des troupes"}},
              {"quantité" : 3, "medicament" : {"nomMolecule" : "MIOu9", "effet" : "Permet de viser plus juste"}},
              {"quantité" : 666, "medicament" : {"nomMolecule" : "HTC", "effet" : "Infecte les pieds"}}
          ],
          "veterinaire" : {"nom" : "Jouglet", "prenom" : "Michel"}
      }
   ]'
);

INSERT INTO Animal values(
  2,
  'Kevin',
  6000,
  20000,
  2003,
  'Baleine',
  '[]'
);

INSERT INTO Animal values(
  3,
  'Vaillant',
  1,
  30,
  2008,
  'Anaconda',
  '[
      {"nom" : "Traitement contre la liquidité des fientes", "debut" : "20 mai 2019", "duree" : 30,
          "prescription" : [
              {"quantité" : 1, "medicament" : {"nomMolecule" : "MOEE", "effet" : "Solidifie les fientes"}}
          ],
          "veterinaire" : {"nom" : "Richard", "prenom" : "Emmanuel"}
      }
   ]'
);




INSERT INTO Clients VALUES(1, '0610203050', 'Dupond', 'Anne', TO_DATE('1995-07-14', 'YYYY-MM-DD'), '50 rue de Nantes 75015 Paris');
INSERT INTO Clients VALUES(2, '0610203050', 'Dupond', 'Anne', TO_DATE('1995-07-14', 'YYYY-MM-DD'), '50 rue de Nantes 75015 Paris');
INSERT INTO Clients VALUES(3, '0614814050', 'Smith', 'Louis', TO_DATE('1980-10-28', 'YYYY-MM-DD'), '25 rue Saint Anne 75009 Paris');

INSERT INTO Proprietaires VALUES (2, 2);
INSERT INTO Proprietaires VALUES (1, 3);
INSERT INTO Proprietaires VALUES (2, 1);




