#!/usr/bin/python3

# http://initd.org/psycopg/docs/usage.html

import psycopg2 as pg
HOST = "tuxa.sme.utc"
USER = "bdd0p071"
PASSWORD = "MFyllo2A"
DATABASE = "dbbdd0p071"
# Open connection

conn = pg.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
# Open a cursor to send SQL commands
cur = conn.cursor()


def insertExecute(sql):
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    conn.commit()
    return True

#Cette fonction parcourt une table avec un id et renvoit l'id le plus grand
def searchId(nomTable):
    sql = "SELECT MAX(id) FROM %s;" %(nomTable)
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    try :
        raw = cur.fetchone()
        if raw:
            while raw:
                print (raw[0])
                return (raw[0])
                raw = cur.fetchone() #renvoit l'id le plus grand de la table s'il existe
        else:
            print("Empty table")
        conn.commit()
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()

# Cette fonction affiche 3 attributs spécifiés de tous les elements contenus dans une table
def afficheAttribut(attribut, table) :
    sql = "SELECT %s FROM %s;" %(attribut, table)
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    try :
        raw = cur.fetchone()
        if raw:
            while raw:
                print (raw[0], raw[1],raw[2])
                print("\n")
                raw = cur.fetchone() #renvoit l'id le plus grand de la table s'il existe
        else:
            print("Empty table")
        conn.commit()
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()

def insertClasses() :
    nomClasse = input("quelle est le nom de la classe que vous souhaitez ajouter ?\n")
    sql = "INSERT INTO Classes(nom) VALUES ('%s');" %(nomClasse)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertEspeces() :
    nomEspece = input("quelle est le nom de l espece que vous souhaitez ajouter ?\n")
    nomClasse = input("quelle est le nom de la classe que vous souhaitez ajouter ?\n")
    sql = "INSERT INTO Especes(nom, classe) VALUES ('%s','%s');" %(nomEspece,nomClasse)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertClients():
    #recherche du client_id le plus grand
    id = searchId("Clients")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False

    nom = input("quelle est le nom du client ?\n")
    prenom = input("quelle est le prenom du client ?\n")
    dateNaissance = input("quelle est la date de naissanhttps://learn.adafruit.com/micropython-basics-loading-modules/import-codece du client ? (format YYYY-MM-DD)\n")
    adresse = input("quelle est l adresse du client ?\n")
    telephone = input("quelle est le telephone du client ?\n")
    sql = "INSERT INTO Clients (id, nom, prenom, date_naissance, adresse, telephone) VALUES (%s,'%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,telephone)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertVeterinaires():
    #recherche du client_id le plus grand
    id = searchId("Veterinaires")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    nom = input("quelle est le nom du veterinaire ?\n")
    prenom = input("quelle est le prenom du veterinaire ?\n")
    dateNaissance = input("quelle est la date de naissance du veterinaire ? (format YYYY-MM-DD)\n")
    adresse = input("quelle est l adresse du veterinaire ?\n")
    specialite = input("quelle est la classe de specialite du veterinaire ?\n")
    telephone = input("quelle est le telephone du veterinaire ?\n")
    sql = "INSERT INTO Veterinaires (id, nom, prenom, date_naissance, adresse, specialite, telephone) VALUES (%s,'%s','%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,specialite,telephone)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertAssistants():
    #recherche du client_id le plus grand
    id = searchId("Assistants")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    nom = input("quelle est le nom de l assistant ?\n")
    prenom = input("quelle est le prenom de l assistant ?\n")
    dateNaissance = input("quelle est la date de naissance de l assistant ? (format YYYY-MM-DD)\n")
    adresse = input("quelle est l adresse de l assistant ?\n")
    specialite = input("quelle est la classe de specialite de l assistant ?\n")
    telephone = input("quelle est le telephone de l assistant ?\n")
    sql = "INSERT INTO Assistants (id, nom, prenom, date_naissance, adresse, specialite, telephone) VALUES (%s,'%s','%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,specialite,telephone)
    if insertExecute(sql) :
        print ("SuccessinsertClient")
        return 1
    else : return 0

def insertAnimaux():
    #recherche du client_id le plus grand
    id = searchId("Animaux")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    nom = input("nom de l animal ?\n")
    dernierPoids = input("dernier poids de l animal ?\n")
    derniereTaille = input("derniere taille de l animal ?\n")
    date_naissance = input("date de naissance de l animal ? \n")
    espece = input("espece de l animal ?\n")
    sql = "INSERT INTO Animaux (id, nom, dernierPoids, derniereTaille, date_naissance, espece) VALUES (%s,'%s',%s,%s,%s,'%s');" %(id, nom, dernierPoids, derniereTaille, date_naissance, espece)
    if insertExecute(sql) :
        print ("Success")
        print ("\n\n" + ('*'*30))
        print ("Ajout d un proprietaire : \n")
        insertProprietaires(id)

def insertProprietaires(idAnimal) :
    print("Qui est le proprietaire ? \n")
    afficheAttribut("id,nom,prenom", "Clients")
    idClient = int(input())
    sql = "INSERT INTO Proprietaires(client, animal) VALUES (%s,%s);" %(idClient,idAnimal)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0


def insertTraitements(nom, debut, duree, veterinaire_id, animal_id) :
    print("blabla")





def insertChoix():

    while True:
        choix=0
        print('*'*30)
        print("1. Ajouter un nouvel animal")
        print("2. Ajouter un nouvel veterinaire")
        print("3. Ajouter un nouvel assistant")
        print("4. Ajouter un nouveau client")
        print("5. Ajouter un nouveau traitement")
        print("\nEt en bonus...")
        print("6. Ajouter une nouvelle classe")
        print("7. Ajouter une nouvelle espece")
        print('*'*30)
        choix = int(input())
        if (choix==1):
            insertAnimaux()
        elif(choix==2):
            insertVeterinaires()
        elif(choix==3):
            insertAssistants()
        elif(choix==4):
            insertClients()
        elif(choix==5):
            insertTraitements()
        elif(choix==6):
            insertClasses()
        elif(choix==7):
            insertEspeces()

        if choix>5 or choix<1:
            return




insertChoix()


# Close connection
conn.close()




"""
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
BASIC FUCNTIONS SECTION
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
"""



#////////// BOUCLE PRINCIPALE /////////////////)
"""
def insertEspece(nom, class_id)
def insertClient(nom, prenom, dateNaissance, adresse, telephone)
def insertPersonnel(nom, prenom, dateNaissance, adresse, telephone, specialite, role)
def insertAnimal(nom, dernierPoids, dernierTaille, dateNaissance, espece)
def insertProprietaire(client_id, animal_id)
def insertMedicament(nomMolecule, effets)
def insertAutorise(medicament_id, espece_id)
def insertTraitement(nom, debut, duree, veterinaire_id, animal_id)
def insertPrescription(traitement_id, medicament_id, quantite)
def insertAnimal(name)
"""
