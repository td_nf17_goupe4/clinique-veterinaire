#!/usr/bin/python3

# http://initd.org/psycopg/docs/usage.html
import os
import psycopg2 as pg
HOST = "tuxa.sme.utc"
USER = "bdd0p071"
PASSWORD = "MFyllo2A"
DATABASE = "dbbdd0p071"
conn = pg.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
cur=conn.cursor()


def Execute(sql) :
    print("SQL : "+sql)
    try:
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    conn.commit()
    return True

################## INSERT #################################
def insertExecute(sql):
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    conn.commit()
    return True

#Cette fonction parcourt une table avec un id et renvoit l'id le plus grand
def searchId(nomTable):
    sql = "SELECT MAX(id) FROM %s;" %(nomTable)
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    try :
        raw = cur.fetchone()
        if raw:
            while raw:
                print (raw[0])
                return (raw[0])
                raw = cur.fetchone() #renvoit l'id le plus grand de la table s'il existe
        else:
            print("Empty table")
        conn.commit()
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()

def afficheAttribut(table, attribut1 = '', attribut2 = '', attribut3 = '') :
    if (attribut2 == '' and attribut3 == '') :
        sql = "SELECT %s FROM %s;" %(attribut1, table)
    elif (attribut3 == '') :
        sql = "SELECT %s,%s FROM %s;" %(attribut1,attribut2, table)
    else :
        sql = "SELECT %s,%s,%s FROM %s;" %(attribut1,attribut2,attribut3,table)
    try:
        print(sql)
        cur.execute(sql)
    except pg.Error as E:
        print(E.pgerror)
        conn.rollback()
        return False
    # Fetch data line by line
    colnames = [desc[0] for desc in cur.description]
    raw_size = 0;
    for colname in colnames :
        text = str(colname.upper())+" "*(15-len(str(colname)))+" |"
        raw_size = raw_size + len(text)+1
        print(text, end=" ")
    print(""*raw_size)
    try:
        raw = cur.fetchone()
        if raw:
            print('-'*(raw_size-1))
            while raw:
                for i in raw:
                    text = str(i)+" "*(15-len(str(i)))+" |"
                    print(text, end=" ")
                print("")
                raw = cur.fetchone()
            print('-'*(raw_size-1))
        else:
            print("Empty table")
        conn.commit()
    except pg.Error as e:
        print("Fetch error", e)
        conn.rollback()

def insertClasses() :
    os.system('cls' if os.name == 'nt' else 'clear')
    nomClasse = str(input("Quel est le nom de la classe que vous souhaitez ajouter ?\n"))
    sql = "INSERT INTO Classes(nom) VALUES ('%s');" %(nomClasse)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertEspeces() :
    os.system('cls' if os.name == 'nt' else 'clear')
    nomEspece = str(input("Quel est le nom de l'espèce que vous souhaitez ajouter ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    print("A quelle classe cette espèce appartient-elle ? \n")
    afficheAttribut("Classes","nom as Classe")
    nomClasse = str(input("Ecrivrez le nom de la classe (attention à la casse)\n"))
    sql = "INSERT INTO Especes(nom, classe) VALUES ('%s','%s');" %(nomEspece,nomClasse)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertClients():
    #recherche du client_id le plus grand
    id = searchId("Clients")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    os.system('cls' if os.name == 'nt' else 'clear')
    nom = str(input("Quel est le nom du client que vous souhaitez ajouter ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    prenom = str(input("Quel est son prénom ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    dateNaissance = str(input("Quelle est sa date de naissance ? (format YYYY-MM-DD)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    adresse = str(input("Quelle est son adresse ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    telephone = int(input("Quel est son numéro de téléphone ?\n"))
    sql = "INSERT INTO Clients (id, nom, prenom, date_naissance, adresse, telephone) VALUES (%s,'%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,telephone)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertPersonnels():
    os.system('cls' if os.name == 'nt' else 'clear')
    print('*'*30)
    print("1. Ajouter un vétérinaire")
    print("2. Ajouter un assistant")
    print("\n3. Revenir en arrière")
    print('*'*30)
    print("Entrez votre choix:", end=" ")
    choix = input()
    if (choix=='1'):
        insertVeterinaires()
    elif(choix=='2'):
        insertAssistants()
    elif(choix=='3'):
        insertChoix()
    else :
        print("Vous avez entré un choix non valide, veuillez recommencer \n")
        insertPersonnels()

def insertVeterinaires():
    #recherche du client_id le plus grand
    id = searchId("Veterinaires")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    os.system('cls' if os.name == 'nt' else 'clear')
    nom = str(input("Quel est le nom du vétérinaire que vous souhaitez ajouter  ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    prenom = str(input("Quel est son prénom ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    dateNaissance = str(input("Quelle est sa date de naissance ? (format YYYY-MM-DD)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    adresse = str(input("Quelle est son adresse ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Quelle est la classe de prédilection du vétérinaire ? \n")
    afficheAttribut("Classes", "nom as classe")
    specialite = str(input("Entrez le nom de la classe (attention à la casse)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    telephone = int(input("Quel est son numéro de téléphone ?\n"))
    sql = "INSERT INTO Veterinaires (id, nom, prenom, date_naissance, adresse, specialite, telephone) VALUES (%s,'%s','%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,specialite,telephone)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertAssistants():
    #recherche du client_id le plus grand
    id = searchId("Assistants")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    os.system('cls' if os.name == 'nt' else 'clear')
    nom = str(input("Quel est le nom de l'assistant que vous souhaitez ajouter ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    prenom = str(input("Quel est son prénom ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    dateNaissance = str(input("Quelle est sa date de naissance ? (format YYYY-MM-DD)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    adresse = str(input("Quelle est son adresse ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Quelle est la classe de prédilection de l'assistant ? \n")
    afficheAttribut("Classes", "nom as classe")
    specialite = str(input("Entrez le nom de la classe (attention à la casse)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    telephone = int(input("Quel est son numéro de téléphone ?\n"))
    sql = "INSERT INTO Assistants (id, nom, prenom, date_naissance, adresse, specialite, telephone) VALUES (%s,'%s','%s','%s','%s','%s',%s);" %(id,nom,prenom,dateNaissance,adresse,specialite,telephone)
    if insertExecute(sql) :
        print ("SuccessinsertClient")
        return 1
    else : return 0

def insertAnimaux():
    #recherche du client_id le plus grand
    id = searchId("Animaux")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    os.system('cls' if os.name == 'nt' else 'clear')
    nom = str(input("Quel est le nom de l'animal que vous souhaitez ajouter ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    dernierPoids = int(input("Quel est son dernier poids connu ? (en grammes)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    derniereTaille = int(input("Quelle est sa dernière taille connue ? (en centimètres)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    date_naissance = input("Connaissez-vous sa date de naissance ? (si oui entrez une année ; si non, tapez NULL) \n")
    os.system('cls' if os.name == 'nt' else 'clear')
    print("A quelle espèce appartient-il ?\n")
    afficheAttribut("Especes","nom as Espece","classe")
    espece = str(input("\nTapez le nom de l'espèce (attention à la casse)\n"))
    sql = "INSERT INTO Animaux (id, nom, dernierPoids, derniereTaille, date_naissance, espece) VALUES (%s,'%s',%s,%s,%s,'%s');" %(id, nom, dernierPoids, derniereTaille, date_naissance, espece)
    if insertExecute(sql) :
        print ("Success")
        os.system('cls' if os.name == 'nt' else 'clear')
        choixUtilisateur = str(input("Souhaitez-vous ajouter un proprietaire correspondant à cet animal ? (o/N)\n"))
        while(choixUtilisateur == 'o') :
            insertProprietaires(id)
            os.system('cls' if os.name == 'nt' else 'clear')
            choixUtilisateur = str(input("Souhaitez-vous ajouter un proprietaire correspondant à cet animal ? (o/N)\n"))
    else : return 0

def insertProprietaires(idAnimal) :
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Entrez le numéro correspondant au proprietaire (référez-vous à la colone id du tableau suivant) \n")
    afficheAttribut("Clients","id","nom","prenom")
    idClient = int(input())
    sql = "INSERT INTO Proprietaires(client, animal) VALUES (%s,%s);" %(idClient,idAnimal)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertTraitements() :
    #recherche du client_id le plus grand
    id = searchId("Traitements")
    if id : id += 1     # s'il existe un id le plus grand, on créé le nouvel id
    else : return False
    os.system('cls' if os.name == 'nt' else 'clear')
    nom = str(input("Quel est le nom du traitement que vous souhaitez ajouter ?\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    debut = str(input("A quelle date débuta ce traitement ? (format YYYY-MM-DD)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    duree = int(input("Quelle est la duree de ce traitement ? (en jour)\n"))
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Entrez le numero correspondant au veterinaire en charge du traitement \n")
    afficheAttribut("Veterinaires","id","nom","prenom")
    idVeterinaire = int(input())
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Entrez le numero correspondant a l'animal traite \n")
    afficheAttribut("Animaux","id","nom","espece")
    idAnimal = int(input())
    sql = "INSERT INTO Traitements(id, nom, debut, duree, veterinaire, animal) VALUES (%s,'%s','%s',%s,%s,%s);" %(id,nom,debut,duree,idVeterinaire,idAnimal)
    if insertExecute(sql) :
        print ("Success")
        os.system('cls' if os.name == 'nt' else 'clear')
        choixUtilisateur = str(input("\nSouhaitez vous ajouter une prescription liée à ce traitement ? (o/N)\n"))
        while(choixUtilisateur == 'o') :
            insertPrescription(id)
            os.system('cls' if os.name == 'nt' else 'clear')
            choixUtilisateur = str(input("\nSouhaitez vous ajouter une prescription liée à ce traitement ? (o/N)\n"))
    else : return 0

def insertPrescription(idTraitement) :
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Entrez le nom du médicament prescrit \n")
    afficheAttribut("Medicaments","nomMolecule","effets")
    medicament = str(input())
    os.system('cls' if os.name == 'nt' else 'clear')
    duree = int(input("\nQuelle quantité de ce médicament l'animal doit-il prendre ? (par jour)\n"))
    sql = "INSERT INTO Prescriptions(traitement, medicament, quantite) VALUES (%s,'%s',%s);" %(idTraitement,medicament,duree)
    if insertExecute(sql) :
        print ("Success")
        return 1
    else : return 0

def insertChoix():

    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        print('*'*30)
        print("Bienvenue dans le menu des insertions de données.\n")
        print("1. Ajouter un nouvel animal")
        print("2. Ajouter un nouveau membre du personnel")
        print("3. Ajouter un nouveau client")
        print("4. Ajouter un nouveau traitement")
        print("\n5. Retourner au menu principal")
        print("\nEt en bonus...")
        print("6. Ajouter une nouvelle classe")
        print("7. Ajouter une nouvelle espece")
        print('*'*30)
        print("Entrez votre choix:", end=" ")
        choix = input()
        if (choix=='1'):
            insertAnimaux()
        elif(choix=='2'):
            insertPersonnels()
        elif(choix=='3'):
            insertClients()
        elif(choix=='4'):
            insertTraitements()
        elif(choix=='5'):
            Main()
        elif(choix=='6'):
            insertClasses()
        elif(choix=='7'):
            insertEspeces()
        else :
            print("Vous avez entré un choix non valide, veuillez recommencer \n")
            insertChoix()
#########################################################



################### AFFICHAGE ET STATISTIQUES ###################
def afficherSql(sql):
    os.system('cls' if os.name == 'nt' else 'clear')
    try:
        cur.execute(sql)
    except pg.Error as e:
        print("Execute: ",e)
        conn.rollback()
        return
    # Fetch data line by line
    colnames = [desc[0] for desc in cur.description]
    raw_size = 0;
    for colname in colnames :
        text = str(colname)+" "*(15-len(str(colname)))+" |"
        raw_size = raw_size + len(text)+1
        print(text, end=" ")

    print(""*raw_size)
    try:
        raw = cur.fetchone()
        if raw:
            print('-'*(raw_size-1))
            while raw:
                for i in raw:
                    text = str(i)+" "*(15-len(str(i)))+" |"
                    print(text, end=" ")
                print("")
                raw = cur.fetchone()
            print('-'*(raw_size-1))
        else:
            print("Empty table")
        conn.commit()
    except pg.Error as e:
        print("Fetch error", e)
        conn.rollback()

def afficherStatistique():
    sql=""
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        choix=0
        print('*'*30)
        print("1.Selection des prescriptions d'un animal (pas demandé mais aide à l'analyse de la statistique")
        print("2.Statistique medicament pour un animal")
        print("3.Statistique quantite prescrite pour un medicament particulier")
        print("4.Poids moyen par espèce ")
        print("5.Taille moyenne par espèce ")
        print("6.Quitter")
        print('*'*30)

        print("Entrez votre choix:", end=" ")
        choix=int(input())
        if (choix==1):
            id=input("Entrez ID d'animal: ")
            sql="SELECT P.medicament, P.quantite FROM Prescriptions P, Traitements T WHERE T.animal = "+str(id)+" AND P.traitement = T.id"
        elif(choix==2):
            id=input("Entrez ID d'animal: ")
            sql="SELECT P.medicament, SUM(P.quantite) FROM Traitements T, Prescriptions P WHERE T.animal = "+str(id)+ "AND P.traitement = T.id GROUP BY P.medicament"
        elif(choix==3):
            afficherSql("select * from Medicaments")
            nomMolecule=input("Entrez nom de la molecule du medicament souhaité : ")
            sql="SELECT SUM(P.quantite) AS quantite_prescrite FROM Prescriptions P WHERE P.medicament = \'"+nomMolecule+"\'"
        elif(choix==4):
            sql="SELECT A.espece, AVG(A.dernierPoids) AS poids_moyen FROM animaux A GROUP BY A.espece;"
        elif(choix==5):
            sql="SELECT A.espece, AVG(A.derniereTaille) AS taille_moyenne FROM animaux A GROUP BY A.espece"
        if choix>5 or choix<1:
            return

        afficherSql(sql)
        sql=""
        input("\nAppuyez pour continuer...")

def afficherTable():
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        sql=""
        print("Tables disponibles //////////\n")
        tables=["classes","especes","clients","veterinaires","assistants","animaux","proprietaires","medicaments","traitements","prescriptions"]
        for table in tables:
            print(table.upper())
        print("\n")
        table=input("Entrez le nom de la table: (Enter q pour quitter)")
        if table == 'q' or table == 'Q':
            break
        if table.lower()  in tables:
            sql="SELECT * FROM "+ table
            os.system('cls' if os.name == 'nt' else 'clear')
            afficherSql(sql)
            input("\nAppuyez pour continuer...")
#############################################################



#################### UPTADE ###############################
def update():
    choix = 0;
    while choix != 3:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("1.Animaux")
        print("2.Clients")
        print("3.Quitter")
        print("\n")
        table=input("Selectionnez une table : ")
        choix = int(table)
        if choix == 1:
            updateAnimal()
        if choix == 2:
            updateClient()

def updateAnimal() :
    try:
        nom='nom'
        dernierPoids='dernierPoids'
        derniereTaille='derniereTaille'
        dateNaissance='date_naissance'
        espece='espece'

        os.system('cls' if os.name == 'nt' else 'clear')
        afficherSql("SELECT * from Animaux")

        text = input("Id de l'animal à modifier : ")
        if text :
            idAnimal = int(text)
        else:
            print("Mauvaise Saisie")
            input("\nAppuyez pour continuer...")
            return False

        print("\n Remplissez les champs, laissez un champ vide si vous ne souhaiter pas modifier la valeur\n")
        text = input("nom (VARCHAR): ")
        if(text):
            nom = "'"+text+"'"
        text = input("dernierPoids (INTEGER) : ")
        if(text and type(text) == int):
            dernierPoids = int(text)
        text = input("derniereTaille (INTEGER) : ")
        if(text and type(text) == int):
            derniereTaille = int(text)
        text = input("date_naissance (INTEGER) : ")
        if(text):
            dateNaissance = int(text)
        text = input("espece (VARCHAR) : ")
        if(text):
            espece = "'"+text+"'"


        sql = "UPDATE Animaux SET nom={}, dernierPoids={}, derniereTaille={}, date_naissance={},  espece={} WHERE id={}".format(nom, dernierPoids, derniereTaille, dateNaissance, espece, idAnimal)
        if Execute(sql):

            afficherSql("SELECT * from Animaux WHERE id="+str(idAnimal))
            print("Animal updated successfully\n")
            input("\nAppuyez pour continuer...")
            return True
        input("\nAppuyez pour continuer...")
    except:
        print("An error occurs, please try again...")
        input("\nAppuyez pour continuer...")
        return False

def updateClient():
    try:
        idClient = -1
        nom = 'nom'
        prenom = 'prenom'
        dateNaissance =  'date_naissance'
        adresse = 'adresse'
        telephone = 'telephone'

        os.system('cls' if os.name == 'nt' else 'clear')
        afficherSql("SELECT * from Clients")

        text = input("Id du client à modifier : ")
        if text :
            idClient = int(text)
        else:
            print("Mauvaise Saisie")
            input("\nAppuyez pour continuer...")
            return False

        print("\n Remplissez les champs, laissez un champ vide si vous ne souhaiter pas modifier la valeur\n")
        text = input("nom (VARCHAR): ")
        if(text):
            nom = "'"+text+"'"
        text = input("prenom (VARCHAR) : ")
        if(text):
            prenom = "'"+text+"'"
        text = input("date_naissance (DATE YYYY-MM-DD ) : ")
        if(text):
            dateNaissance = "TO_DATE('"+text+"', 'YYYY-MM-DD')"
        text = input("adresse (VARCHAR) : ")
        if(text):
            adresse = "'"+text+"'"
        text = input("telephone (INTEGER) : ")
        if(text):
            telephone = int(text)

        sql = "UPDATE Clients SET nom={}, prenom={}, date_naissance={}, adresse={}, telephone={} WHERE id={}".format( nom, prenom, dateNaissance, adresse, telephone, idClient)
        if Execute(sql):
            afficherSql("SELECT * from Clients WHERE id="+str(idClient))
            print("Client updated successfully")
            input("\nAppuyez pour continuer...")
            return True
        input("\nAppuyez pour continuer...")
    except:
        print("An error occurs, please try again...")
        input("\nAppuyez pour continuer...")
        return False
    
##########################################################

####################### DELETE ############################
def ExecuteList(sqls) :
    for sql in sqls:
        print('SQL: ' + sql)
        try:
            cur.execute(sql)
        except pg.Error as E:
            print(E.pgerror)
            conn.rollback()
            return False
    conn.commit()
    return True

def deleteClient(idClient):
    sqls = []    
    sqls.append("DELETE FROM Proprietaires WHERE client={};".format(idClient))
    sqls.append("DELETE FROM Clients WHERE id={};".format(idClient))
    if(ExecuteList(sqls)):
        print("Delete Client Succes")
    else:
        print("Delete Client Fail")

def deletePersonnel(idPersonnel, isVeterinaires=False):
    sqls = []
    table = 'Assistants'
    if(isVeterinaires):
        table = 'Veterinaires'
        sql = "SELECT id FROM Traitements WHERE veterinaire={};".format(idPersonnel)
        cur.execute(sql)
        raw = cur.fetchone()
        while raw:
            sqls.append("DELETE FROM Prescriptions WHERE traitement={}".format(raw[0]))
        raw = cur.fetchone()
        sqls.append("DELETE FROM Traitements WHERE veterinaire={}".format(idPersonnel))
    sqls.append("DELETE FROM {} WHERE id={};".format(table, idPersonnel))
    if(ExecuteList(sqls)):
        print("Delete {} Succes".format(table))
    else:
        print("Delete Veterinaire Fail")
    
def deleteAnimal(idAnimal):
    sqls = []
    sql = "SELECT id FROM Traitements WHERE animal={};".format(idAnimal)
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        sqls.append("DELETE FROM Prescriptions WHERE traitement={}".format(raw[0]))
        raw = cur.fetchone()
    sqls.append("DELETE FROM Proprietaires WHERE animal={};".format(idAnimal))
    sqls.append("DELETE FROM Traitements WHERE animal={};".format(idAnimal))
    sqls.append("DELETE FROM Animaux WHERE id={};".format(idAnimal))
    if(ExecuteList(sqls)):
        print("Delete Animal Succes")
    else:
        print("Delete Animal Fail")

def delete():
    
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        print('*'*30)
        print("1.Animaux")
        print("2.Clients")
        print("3.Veterinaires")
        print("4.Assistants")
        print("5.Quitter")
        print("Entrez votre choix:", end=" ")
        choix=input()
        if choix=="1":
            afficherSql("SELECT * from Animaux")
            text = input("Id de l'animal à supprimer : ")
            if text :
                idAnimal = int(text)
                deleteAnimal(idAnimal)
            else:
                print("Mauvaise Saisie")
                input("\nAppuyez pour continuer...")
        elif choix=='2':
            afficherSql("SELECT * from Clients")
            text = input("Id du client à supprimer : ")
            if text :
                idClient = int(text)
                sql = "SELECT animal FROM Proprietaires WHERE client={};".format(idClient)
                cur.execute(sql)
                results = []
                raw = cur.fetchone()
                while raw:
                    results.append(raw[0])
                    raw = cur.fetchone()
                for i in results:
                    deleteAnimal(i)
                deleteClient(idClient)
            else:
                print("Mauvaise Saisie")
                input("\nAppuyez pour continuer...")
        elif choix=='3':
            afficherSql("SELECT * from Veterinaires;")
            text = input("Id du veterinaire à supprimer : ")
            if text :
                idPersonnel = int(text)
                deletePersonnel(idPersonnel,True)
            else:
                print("Mauvaise Saisie")
                input("\nAppuyez pour continuer...")
            
        elif choix=='4':
            afficherSql("SELECT * from Assistants;")
            text = input("Id du assistant à supprimer : ")
            if text :
                idPersonnel = int(text)
                deletePersonnel(idPersonnel,False)
            else:
                print("Mauvaise Saisie")
                input("\nAppuyez pour continuer...")
        elif choix=='5':
            break
        else :
            print("Vous avez entré un choix non valide, veuillez recommencer \n")
            Main()
    
    return    
##########################################################

####################### MAIN ############################
def Main():

    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        print('*'*30)
        print("Bienvenue dans le menu principal.\n")
        print("1.Afficher le contenu d'une table")
        print("2.Afficher les statistiques")
        print("3.Inserer dans une table")
        print("4.Modifier un tuple")
        print("5.Supprimer un tuple")
        print("\n6.Quitter")
        print('*'*30)
        print("Entrez votre choix:", end=" ")
        choix=input()
        if choix=="1":
            afficherTable()
        elif choix=='2':
            afficherStatistique()
        elif choix=='3':
            insertChoix()
        elif choix=='4':
            update()
        elif choix=='5':
            delete()
        elif choix=='6':
            break
        else :
            print("Vous avez entré un choix non valide, veuillez recommencer \n")
            Main()



Main()
